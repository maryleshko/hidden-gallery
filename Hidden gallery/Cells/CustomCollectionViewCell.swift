import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
   
    // MARK: - IBOutlets

    @IBOutlet weak var customImageView: UIImageView!
    
    // MARK: - Flow functions

    func setup(with image: UIImage?) {
        customImageView.image = image
        customImageView.contentMode = .scaleAspectFill
    }
}
