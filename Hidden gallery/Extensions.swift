import Foundation
import UIKit

extension UIView {
    func roundCorners(radius: CGFloat = 20) {
        self.layer.cornerRadius = radius
    }
    
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true, cornerRadius: CGFloat = 20) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        
        layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: cornerRadius).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    func addGradient(colors: [CGColor], cornerRadius: CGFloat = 20) {
        let gradient = CAGradientLayer()
        
        gradient.colors = colors
        
        // Gradient from left to right
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        gradient.frame = self.bounds
        gradient.cornerRadius = cornerRadius
        self.layer.addSublayer(gradient)
    }
    
    
}


