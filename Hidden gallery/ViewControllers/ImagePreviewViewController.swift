import UIKit

// MARK: - Protocols

protocol ImagePreviewViewControllerProtocol: AnyObject {
    func updateData(_ array: [PhotoObject])
}

class ImagePreviewViewController: UIViewController {
    
    // MARK: - Public Properties

    var myCollectionView: UICollectionView!
    var imgArray = [PhotoObject]()
    var passedContentOffset = IndexPath()
    var index = Int()
    var isMenuHidden = Bool()
    weak var delegate: ImagePreviewViewControllerProtocol?
    let makeFavoriteButton = UIButton(type: .roundedRect)

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // MARK: - Lifecycle functions

    override func viewDidLoad() {
        super.viewDidLoad()
        

        self.imgArray = PhotoObjectManager.shared.getSettings()
        
        self.view.backgroundColor=UIColor.black
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        
        myCollectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        myCollectionView.delegate = self
        myCollectionView.dataSource = self
        myCollectionView.register(ImagePreviewFullViewCell.self, forCellWithReuseIdentifier: "Cell")
        myCollectionView.isPagingEnabled = true
        myCollectionView.autoresizingMask = UIView.AutoresizingMask(rawValue: UIView.AutoresizingMask.RawValue(UInt8(UIView.AutoresizingMask.flexibleWidth.rawValue) | UInt8(UIView.AutoresizingMask.flexibleHeight.rawValue)))
        
        self.view.addSubview(myCollectionView)
       
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
            self.myCollectionView.scrollToItem(at: self.passedContentOffset, at: .centeredHorizontally, animated: false)
        
        
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        guard let flowLayout = myCollectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        flowLayout.itemSize = myCollectionView.frame.size
        flowLayout.invalidateLayout()
        
        myCollectionView.collectionViewLayout.invalidateLayout()
       
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        let offset = myCollectionView.contentOffset
        let width  = myCollectionView.bounds.size.width
        
        let index = round(offset.x / width)
        let newOffset = CGPoint(x: index * size.width, y: offset.y)
        
        myCollectionView.setContentOffset(newOffset, animated: false)
        
        coordinator.animate(alongsideTransition: { (context) in
            self.myCollectionView.reloadData()
            
            self.myCollectionView.setContentOffset(newOffset, animated: false)
        }, completion: nil)
    }
}

    // MARK: - Extension UICollectionView

extension ImagePreviewViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgArray.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? ImagePreviewFullViewCell else {return UICollectionViewCell() }
        cell.imageView.image = FilesManager.shared.loadImageFromDiskWith(fileName: imgArray[indexPath.item].uuid ?? "")
        cell.makeFavoriteButton.isSelected =  imgArray[indexPath.item].isFavorite ?? false
        cell.index = indexPath.item
        self.index = indexPath.item
        self.makeFavoriteButton.isSelected = imgArray[indexPath.item].isFavorite ?? false
        cell.makeFavoriteButton.tag = indexPath.item
        cell.shareButton.tag = indexPath.item
        cell.showCommentButton.tag = indexPath.item
        cell.deletePhotoButton.tag = indexPath.item
        cell.delegate = self
        cell.isMenuHidden = self.isMenuHidden
        print(indexPath.item)
        return cell
    }
}

    // MARK: - Extension ImagePreviewFullViewCellProtocol

extension ImagePreviewViewController: ImagePreviewFullViewCellProtocol {
    func hidingMenu(_ isHidden: Bool) {
        self.isMenuHidden = isHidden
    }
    
    func hidingGallery() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func deletePhoto(_ index: Int) {
        let alertCtrl = UIAlertController(title: "Delete photo", message: "Are you sure? Do you want to delete this photo?", preferredStyle: .alert)
        let deleteAction = UIAlertAction(title: "DELETE", style: .default) { (_) in
            guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
                        return
            }
            if let imageName = self.imgArray[index].uuid {
                let imagePath = "\(documentsDirectory.absoluteURL.absoluteString)\(imageName)".replacingOccurrences(of: "file://", with: "")
                if FileManager.default.fileExists(atPath: imagePath) {
                    do {
                        print("here")
                        try FileManager.default.removeItem(atPath: imagePath)
                        print(self.imgArray)
                        self.imgArray.remove(at: index)
                        print(self.imgArray)
                 PhotoObjectManager.shared.setSettings(self.imgArray)
                        self.myCollectionView.reloadData()
                        self.delegate?.updateData(self.imgArray)
                    } catch {
                        print(error)
                    }
                }
            }
        }
        let cancelAction = UIAlertAction(title: "CANCEL", style: .cancel) { (_) in
            self.dismiss(animated: true, completion: nil)
        }
        alertCtrl.addAction(deleteAction)
        alertCtrl.addAction(cancelAction)
        self.present(alertCtrl, animated: true, completion: nil)
         
    }
    
    func showComment(_ index: Int) {
        if self.imgArray[index].comment == "" {
            let alertCtrl = UIAlertController(title: "COMMENT", message: "There is no comment for this photo. You can add it", preferredStyle: .actionSheet)
            
            let addAction = UIAlertAction(title: "Add comment", style: .default) { (_) in
                let secondAlertCtrl = UIAlertController(title: "COMMENT", message: "Fill in the field", preferredStyle: .alert)
                secondAlertCtrl.addTextField { (text) in
                    text.textColor = .black
                    text.placeholder = "Enter the comment"
                }
                let saveAction = UIAlertAction(title: "SAVE", style: .default) { (_) in
                    
                    let text = secondAlertCtrl.textFields?[0] as? UITextField
                    self.imgArray[index].comment = text?.text
                    PhotoObjectManager.shared.setSettings(self.imgArray)
                }
                let secondCancelAction = UIAlertAction(title: "CANCEL", style: .cancel) { (_) in
                    self.dismiss(animated: true, completion: nil)
                }
                secondAlertCtrl.addAction(saveAction)
                secondAlertCtrl.addAction(secondCancelAction)
                self.present(secondAlertCtrl, animated: true, completion: nil)
            }
            let cancelAction = UIAlertAction(title: "CANCEL", style: .cancel) { (_) in
                self.dismiss(animated: true, completion: nil)
            }
            alertCtrl.addAction(addAction)
            alertCtrl.addAction(cancelAction)
            self.present(alertCtrl, animated: true, completion: nil)
        } else {
            let alertCtrl = UIAlertController(title: "COMMENT", message: "You can change or delete comment", preferredStyle: .alert)
            
            alertCtrl.addTextField { (text) in
                text.text = self.imgArray[index].comment
            }
            
            let changeAction = UIAlertAction(title: "Change comment", style: .default) { (_) in
                let text = alertCtrl.textFields?[0] as? UITextField
                self.imgArray[index].comment = text?.text
                PhotoObjectManager.shared.setSettings(self.imgArray)
                
            }
            let cancelAction = UIAlertAction(title: "CANCEL", style: .cancel) { (_) in
                self.dismiss(animated: true, completion: nil)
            }
            let deleteActiion = UIAlertAction(title: "Delete comment", style: .default) { (_) in
                self.imgArray[index].comment = ""
                PhotoObjectManager.shared.setSettings(self.imgArray)
                
                let okayAlertCtrl = UIAlertController(title: "COMMENT", message: "YOU HAVE DELETED COMMENT", preferredStyle: .alert)
                let okayAction = UIAlertAction(title: "OK", style: .default) { (_) in
                    self.dismiss(animated: true, completion: nil)
                }
                
                okayAlertCtrl.addAction(okayAction)
                self.present(okayAlertCtrl, animated: true, completion: nil)
            }
            alertCtrl.addAction(changeAction)
            alertCtrl.addAction(deleteActiion)
            alertCtrl.addAction(cancelAction)
            self.present(alertCtrl, animated: true, completion: nil)
        }
    }
    
    func sharePhoto(_ index: Int) {
        let shareController = UIActivityViewController(activityItems: [FilesManager.shared.loadImageFromDiskWith(fileName: imgArray[index].uuid ?? "")], applicationActivities: nil)
        shareController.completionWithItemsHandler = {_, bool, _, _ in
            if bool {
                print("Success")
            }
        }
        self.present(shareController, animated: true, completion: nil)
    }
    
    func update(_ index: Int) {
        if self.imgArray[index].isFavorite == true {
            self.imgArray[index].isFavorite = false
            PhotoObjectManager.shared.setSettings(imgArray)
            myCollectionView.reloadData()
        } else {
            self.imgArray[index].isFavorite = true
            PhotoObjectManager.shared.setSettings(imgArray)
            myCollectionView.reloadData()
        }
    }
}

