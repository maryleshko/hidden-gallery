import UIKit
import SwiftyKeychainKit

class AuthorizationViewController: UIViewController {
    
    // MARK: - IBOutlets

    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var frameImageView: UIImageView!
    @IBOutlet weak var openPasswordAlertButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    // MARK: - Public Properties

    let keychain = Keychain(service: "com.maryl.Homework-11--Galery-")
    let myKey = KeychainKey<String>(key: "accessToken")
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // MARK: - Lifecycle functions

    override func viewDidLoad() {
        super.viewDidLoad()
        
        try? keychain.set("123", for: myKey)
        
        setSettings()
        
    }
    
    // MARK: - IBActions

    @IBAction func openPasswordAlertButton(_ sender: UIButton) {
        let alertCtrl = UIAlertController(title: "PASSWORD", message: "You have to enter the password", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "ENTER", style: .default) { (_) in
            let secondAlertCtrl = UIAlertController(title: "YOUR PASSWORD", message: nil, preferredStyle: .alert)
            secondAlertCtrl.addTextField { (text) in
                
                text.textColor = .black
                text.placeholder = "Enter the password"
                text.isSecureTextEntry = true
            }
            let secondOkAction = UIAlertAction(title: "OK", style: .default) { (_) in
                
                let text = secondAlertCtrl.textFields?[0] as? UITextField
                guard let password = try? self.keychain.get(self.myKey) else {return}
                if text?.text != nil && text?.text == password {
                    let thirdAlertCtrl = UIAlertController(title: "EVERYTHING IS OKAY!", message: "You have written the right password. Thank you!", preferredStyle: .alert)
                    let thirdOkAction = UIAlertAction(title: "OK", style: .default) { (_) in
                        
                        guard let galleryController = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController else {
                            return
                        }
                        galleryController.modalPresentationStyle = .fullScreen                        
                        self.navigationController?.pushViewController(galleryController, animated: true)
                        
                        
                    }
                    thirdAlertCtrl.addAction(thirdOkAction)
                    self.present(thirdAlertCtrl, animated: true)
                } else {
                    let fourthAlertCtrl = UIAlertController(title: "WRONG!", message: "You have written the wrong password!", preferredStyle: .alert)
                    let tryAction = UIAlertAction(title: "TRY AGAIN", style: .default) { (_) in
                        self.present(secondAlertCtrl, animated: true)
                    }
                    fourthAlertCtrl.addAction(tryAction)
                    self.present(fourthAlertCtrl, animated: true, completion: .some({
                        text?.text = nil
                    }))
                }
            }
            let secondCancelAction = UIAlertAction(title: "CANCEL", style: .cancel) { (_) in
                exit(0)
            }
            
            secondAlertCtrl.addAction(secondCancelAction)
            secondAlertCtrl.addAction(secondOkAction)
            self.present(secondAlertCtrl, animated: true)
        }
        let cancelAction = UIAlertAction(title: "CANCEL", style: .cancel) { (_) in
            exit(0)
        }
        alertCtrl.addAction(okAction)
        alertCtrl.addAction(cancelAction)
        self.present(alertCtrl, animated: true)
    }
    
    // MARK: - Flow functions

    func setSettings() {
        
        addParallaxToView(view: backgroundImageView, magnitude: 30)
        backgroundImageView.image = UIImage(named: "gallery")
        backgroundImageView.contentMode = .scaleAspectFill
        
        titleLabel.text = "Hidden gallery"
        titleLabel.font = UIFont(name: Constants.font, size: 50)
        titleLabel.textColor = .white
        
        openPasswordAlertButton.setTitle("Enter the password", for: .normal)
        openPasswordAlertButton.setTitleColor(.white, for: .normal)
        openPasswordAlertButton.titleLabel?.font = UIFont(name: Constants.font, size: 35)
        

    }
    
    func addParallaxToView(view: UIView, magnitude: Float) {
        let horizontal = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        horizontal.minimumRelativeValue = -magnitude
        horizontal.maximumRelativeValue = magnitude
        
        let vertical = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
        vertical.minimumRelativeValue = -magnitude
        vertical.maximumRelativeValue = magnitude
        
        let group = UIMotionEffectGroup()
        group.motionEffects = [horizontal, vertical]
        view.addMotionEffect(group)
    }
}
