import UIKit
import SwiftyKeychainKit

// MARK: - Protocols

protocol MainViewControllerProtocol {
    func choosePhoto(index: Int)
}
class MainViewController: UIViewController {
    
    // MARK: - IBOutlets

    @IBOutlet weak var buttonChoosedPhoto: UIButton!
    @IBOutlet weak var buttonOpenedGallery: UIButton!
    @IBOutlet weak var containerImageView: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - Public Properties

    var imageName: String = ""
    var galleryArray = [PhotoObject]()

    let keychain = Keychain(service: "com.maryl.Homework-11--Galery-")
    let myKey = KeychainKey<String>(key: "accessToken")
    let imagePicker = UIImagePickerController()

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // MARK: - Lifecycle functions

    override func viewDidLoad() {
        super.viewDidLoad()

        self.imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
        
        self.galleryArray = PhotoObjectManager.shared.getSettings()
        
        try? keychain.set("123", for: myKey)
        
        if galleryArray == [PhotoObject]() {
            for element in ArrayManager.shared.namesArray {
                if let image = element {
                    let photoObject = PhotoObject()
                    FilesManager.shared.saveImage(image: image, userImage: photoObject)
                    print(photoObject.imagePath)
                    galleryArray.append(photoObject)
                    PhotoObjectManager.shared.setSettings(galleryArray)
                }
            }
        }
        self.galleryArray = PhotoObjectManager.shared.getSettings()
        
        setSettings()
        
    }
    
    // MARK: - IBActions

    @IBAction func buttonChoosedPhoto(_ sender: UIButton) {
        let alertCtrl = UIAlertController(title: "CHOOSE PHOTO", message: "Do you want to open camera or photo library?", preferredStyle: .actionSheet)
        let libraryAction = UIAlertAction(title: "Photo library", style: .default) { (_) in
            self.pickPhoto()
        }
        let cameraAcrion = UIAlertAction(title: "Open camera", style: .default) { (_) in
            self.pickCamera()
        }
        let cancelAction = UIAlertAction(title: "CANCEL", style: .cancel)
        alertCtrl.addAction(libraryAction)
        alertCtrl.addAction(cameraAcrion)
        alertCtrl.addAction(cancelAction)
        self.present(alertCtrl, animated: true, completion: nil)
    }
    @IBAction func buttonOpenedGallery(_ sender: UIButton) {
        
        let galleryController = ImagePreviewViewController()
        galleryController.modalPresentationStyle = .fullScreen
        galleryController.index = 0
        galleryController.delegate = self as? ImagePreviewViewControllerProtocol
        self.navigationController?.pushViewController(galleryController, animated: true)
    }
    
    // MARK: - Flow functions

    func pickCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            self.imagePicker.sourceType = .camera
            present(self.imagePicker, animated: true, completion: nil)
        }
    }
  
    func pickPhoto() {
        self.imagePicker.sourceType = .photoLibrary
        present(self.imagePicker, animated: true, completion: nil)
    }
    func setSettings() {
        buttonChoosedPhoto.roundCorners()
        buttonOpenedGallery.roundCorners()

        buttonOpenedGallery.setTitle("Gallery", for: .normal)
        buttonOpenedGallery.titleLabel?.font = UIFont(name: Constants.font, size: 20)
        buttonChoosedPhoto.setTitle("Choose photo", for: .normal)
        buttonChoosedPhoto.titleLabel?.font = UIFont(name: Constants.font, size: 20)
    }
}

    // MARK: - Extension UIImagePickerController

extension MainViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            let photoObject = PhotoObject()
            FilesManager.shared.saveImage(image: pickedImage, userImage: photoObject)
            print(photoObject.imagePath)
            galleryArray.append(photoObject)
            PhotoObjectManager.shared.setSettings(galleryArray)
        }
        picker.dismiss(animated: true, completion: nil)
        collectionView.reloadData()
    }
    
}

    // MARK: - Extension UICollectionView

extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return galleryArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomCollectionViewCell", for: indexPath) as? CustomCollectionViewCell else {
            return UICollectionViewCell()
        }
        guard let uuid = galleryArray[indexPath.item].uuid else { return UICollectionViewCell()}
        let image = FilesManager.shared.loadImageFromDiskWith(fileName: uuid)
        cell.customImageView.image = image
        cell.customImageView.contentMode = .scaleAspectFill
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth = self.view.frame.width
        let itemsPerRow: CGFloat = 3
        let paddingWidth = Constants.cellSize * (itemsPerRow + 1)
        let availableWidth = screenWidth - paddingWidth
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: Constants.cellSize, left: Constants.cellSize, bottom: Constants.cellSize, right: Constants.cellSize)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return Constants.cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return Constants.cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let galleryController = ImagePreviewViewController()
        galleryController.modalPresentationStyle = .fullScreen
        galleryController.passedContentOffset = indexPath
        galleryController.index = indexPath.item
        galleryController.delegate = self as? ImagePreviewViewControllerProtocol
        self.navigationController?.pushViewController(galleryController, animated: true)
        
    }
}

    // MARK: - Extension ImagePreviewViewControllerProtocol

extension MainViewController: ImagePreviewViewControllerProtocol {
    func updateData(_ array: [PhotoObject]) {
        self.galleryArray = array
        self.collectionView.reloadData()
    }
}
