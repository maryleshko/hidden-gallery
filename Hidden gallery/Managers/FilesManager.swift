import Foundation
import UIKit

class FilesManager: NSObject {
    static let shared = FilesManager()
    private override init() {}
    
    func saveImage(image: UIImage, userImage: PhotoObject){
        guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            return
        }
        let name = UUID().uuidString
        let fileURL = documentsDirectory.appendingPathComponent(name)
        guard let data = image.jpegData(compressionQuality: 1) else {return}
        userImage.imagePath = fileURL
        userImage.uuid = name
        userImage.isFavorite = false
        userImage.comment = ""
        
        do {
            try data.write(to: fileURL)
            print("OK")
        } catch let error {
            print("ERROR", error)
        }
    }
    func loadImageFromDiskWith(fileName: String) -> UIImage? {
        
        let documentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let userDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        
        let paths = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)
        
        if let dirPath = paths.first {
            let imageUrl = URL(fileURLWithPath: dirPath).appendingPathComponent(fileName)
            let image = UIImage(contentsOfFile: imageUrl.path)
            return image
        }
        return nil
    }
}
