import UIKit

// MARK: - Protocols

protocol ImagePreviewFullViewCellProtocol: AnyObject {
    func update(_ index: Int)
    func sharePhoto(_ index: Int)
    func showComment(_ index: Int)
    func deletePhoto(_ index: Int)
    func hidingGallery()
    func hidingMenu(_ isHidden: Bool)
}

class ImagePreviewFullViewCell: UICollectionViewCell, UIScrollViewDelegate {
    
    // MARK: - Public Properties

    var scrollImage: UIScrollView!
    var imageView: UIImageView!
    var index = Int()
    var imageArray = [PhotoObject]()
    var topMenu = UIView()
    var bottomMenu = UIView()
    var backToMenuButton = UIButton()
    var showCommentButton = UIButton()
    var deletePhotoButton = UIButton()
    var isMenuHidden = Bool()
    weak var delegate: ImagePreviewFullViewCellProtocol?
    
    let makeFavoriteButton = UIButton(type: .roundedRect)
    let shareButton = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.isMenuHidden = true
        
        self.imageArray = PhotoObjectManager.shared.getSettings()
        scrollImage = UIScrollView()
        scrollImage.delegate = self
        scrollImage.alwaysBounceVertical = false
        scrollImage.alwaysBounceHorizontal = false
        scrollImage.showsVerticalScrollIndicator = true
        scrollImage.flashScrollIndicators()
        scrollImage.minimumZoomScale = 1.0
        scrollImage.maximumZoomScale = 4.0
        
        let doubleTapGest = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTapScrollView(recognizer:)))
        doubleTapGest.numberOfTapsRequired = 2
        scrollImage.addGestureRecognizer(doubleTapGest)
        
        self.addSubview(scrollImage)
        
        imageView = UIImageView()
        scrollImage.addSubview(imageView!)
        imageView.contentMode = .scaleAspectFit
        
        topMenu.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: 50)
        topMenu.backgroundColor = .black
        self.addSubview(topMenu)
        
        
        backToMenuButton.frame = CGRect(x: 10, y: 10, width: 30, height: 30)
        backToMenuButton.setBackgroundImage(UIImage(named: "back"), for: .normal)
        backToMenuButton.addTarget(self, action: #selector(self.backToMenuButton(_:)), for: .touchUpInside)
        self.addSubview(backToMenuButton)
        
        bottomMenu.frame = CGRect(x: 0, y: self.frame.size.height - 50, width: self.frame.size.width, height: 50)
        bottomMenu.backgroundColor = .black
        self.addSubview(bottomMenu)
        
        makeFavoriteButton.frame = CGRect(x: self.bottomMenu.frame.size.width / 2 - 25, y: self.frame.size.height - 50, width: 40, height: 40)
        makeFavoriteButton.setTitleColor(.white, for: .normal)
        makeFavoriteButton.isSelected =  imageArray[index].isFavorite ?? false
        makeFavoriteButton.setBackgroundImage(UIImage(named: "whiteHeart"), for: .normal)
        makeFavoriteButton.setBackgroundImage(UIImage(named: "redHeart"), for: .selected)
        
        makeFavoriteButton.addTarget(self, action: #selector(self.makeFavoriteButton(_:)), for: .touchUpInside)
        self.addSubview(makeFavoriteButton)
        
        shareButton.setBackgroundImage(UIImage(named: "share"), for: .normal)
        shareButton.frame = CGRect(x: 20, y: self.frame.size.height - 50, width: 25, height: 30)
        
        shareButton.addTarget(self, action: #selector(self.shareButton(_:)), for: .touchUpInside)
        self.addSubview(shareButton)
        
        showCommentButton.frame = CGRect(x: self.frame.size.width - 50, y: self.frame.size.height - 50, width: 30, height: 30)
        showCommentButton.addTarget(self, action: #selector(self.showCommentButton(_:)), for: .touchUpInside)
        showCommentButton.setBackgroundImage(UIImage(named: "comment"), for: .normal)
        self.addSubview(showCommentButton)
        
        deletePhotoButton.frame = CGRect(x: self.frame.size.width - 50, y: 10, width: 30, height: 30)
        deletePhotoButton.setBackgroundImage(UIImage(named: "trash"), for: .normal)
        deletePhotoButton.addTarget(self, action: #selector(self.deletePhotoButton(_:)), for: .touchUpInside)
        self.addSubview(deletePhotoButton)
        
        let oneTapGest = UITapGestureRecognizer(target: self, action: #selector(oneTapRecognizer(recognizer:)))
        oneTapGest.numberOfTapsRequired = 1
        self.addGestureRecognizer(oneTapGest)
        
        if isMenuHidden == true {
            self.hidingMenu()
        } else {
            self.showMenu()
        }
        
        let swipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeRecognizer(recognizer:)))
        swipeRecognizer.direction = .down
        self.addGestureRecognizer(swipeRecognizer)
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        scrollImage.frame = self.bounds
        imageView.frame = self.bounds
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        scrollImage.setZoomScale(1, animated: true)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    // MARK: - IBActions

    @IBAction func backToMenuButton(_ sender: AnyObject) {
        self.delegate?.hidingGallery()
    }
    @IBAction func deletePhotoButton(_ sender: AnyObject) {
        self.delegate?.deletePhoto(deletePhotoButton.tag)
    }
    @IBAction func showCommentButton(_ sender: AnyObject) {
        self.delegate?.showComment(showCommentButton.tag)
    }
    @objc func swipeRecognizer(recognizer: UISwipeGestureRecognizer) {
        self.delegate?.hidingGallery()
    }
    @objc func oneTapRecognizer(recognizer: UITapGestureRecognizer) {
        if self.topMenu.frame.origin.y == 0 {
            UIView.animate(withDuration: 0.5) {
                self.hidingMenu()
            }
        } else {
            UIView.animate(withDuration: 0.5) {
                self.showMenu()
            }
        }
    }
    
    @objc func handleDoubleTapScrollView(recognizer: UITapGestureRecognizer) {
        if scrollImage.zoomScale == 1 {
            scrollImage.zoom(to: zoomRectForScale(scale: scrollImage.maximumZoomScale, center: recognizer.location(in: recognizer.view)), animated: true)
        } else {
            scrollImage.setZoomScale(1, animated: true)
        }
    }
    @IBAction func makeFavoriteButton(_ sender: AnyObject) {
        self.delegate?.update(makeFavoriteButton.tag)
    }
    @IBAction func shareButton(_ sender: AnyObject) {
        self.delegate?.sharePhoto(shareButton.tag)
    }
    
    // MARK: - Flow functions

    func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
        var zoomRect = CGRect.zero
        zoomRect.size.height = imageView.frame.size.height / scale
        zoomRect.size.width  = imageView.frame.size.width  / scale
        let newCenter = imageView.convert(center, from: scrollImage)
        zoomRect.origin.x = newCenter.x - (zoomRect.size.width / 2.0)
        zoomRect.origin.y = newCenter.y - (zoomRect.size.height / 2.0)
        return zoomRect
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
    
    func hidingMenu() {
        self.topMenu.frame.origin.y -= 50
        self.backToMenuButton.frame.origin.y -= 50
        self.bottomMenu.frame.origin.y = self.frame.size.height
        self.makeFavoriteButton.frame.origin.y = self.frame.size.height
        self.shareButton.frame.origin.y = self.frame.size.height
        self.deletePhotoButton.frame.origin.y -= 50
        self.showCommentButton.frame.origin.y = self.frame.size.height
        self.isMenuHidden = true
        self.delegate?.hidingMenu(true)
    }
    func showMenu() {
        self.topMenu.frame.origin.y = 0
        self.backToMenuButton.frame.origin.y = 10
        self.bottomMenu.frame.origin.y = self.frame.size.height - 50
        self.makeFavoriteButton.frame.origin.y = self.frame.size.height - 50
        self.shareButton.frame.origin.y = self.frame.size.height - 50
        self.deletePhotoButton.frame.origin.y = 10
        self.showCommentButton.frame.origin.y = self.frame.size.height - 50
        self.delegate?.hidingMenu(false)

    }
    
    
}
