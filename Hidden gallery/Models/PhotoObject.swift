import UIKit

class PhotoObject: NSObject, Codable {
    
    var uuid: String?
    var imagePath: URL?
    var isFavorite: Bool?
    var comment: String?
    

    init(isFavorite: Bool?, comment: String?, uuid: String?, imagePath: URL?) {
            self.isFavorite = isFavorite
            self.comment = comment
            self.uuid = uuid
        self.imagePath = imagePath
        }
    
    public enum CodingKeys: String, CodingKey {
        case isFavorite, comment, uuid, imagePath
    }
    public override init() {
//        uuid = UUID().uuidString
    }
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.isFavorite = try container.decodeIfPresent(Bool.self, forKey: .isFavorite)
        self.comment = try container.decodeIfPresent(String.self, forKey: .comment)
        self.uuid = try container.decodeIfPresent(String.self, forKey: .uuid)
        self.imagePath = try container.decodeIfPresent(URL.self, forKey: .imagePath)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.isFavorite, forKey: .isFavorite)
        try container.encode(self.comment, forKey: .comment)
        try container.encode(self.uuid, forKey: .uuid)
        try container.encode(self.imagePath, forKey: .imagePath)
    }
    
}

//extension UserDefaults {
//
//    func set<T: Encodable>(encodable: T, forKey key: String) {
//        if let data = try? JSONEncoder().encode(encodable) {
//            set(data, forKey: key)
//        }
//    }
//
//    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
//        if let data = object(forKey: key) as? Data,
//            let value = try? JSONDecoder().decode(type, from: data) {
//            return value
//        }
//        return nil
//    }
//}
