import UIKit

class Constants {
    static let firstImage = UIImage(named: "firstImage")
    static let secondImage = UIImage(named: "secondImage")
    static let thirdImage = UIImage(named: "thirdImage")
    static let fourthImage = UIImage(named: "fourthImage")
    static let fifthImage = UIImage(named: "fifthImage")
    static let cellSize: CGFloat = 20
    static let font = "Playlist-Script"
    
}

