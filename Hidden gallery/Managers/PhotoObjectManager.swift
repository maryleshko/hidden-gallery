import Foundation
import UIKit

class PhotoObjectManager {
    static let shared = PhotoObjectManager()
    
    private var defaultSettings = PhotoObject(isFavorite: false, comment: "", uuid: "", imagePath: nil)
    let key = "photo"
    
    func getSettings() -> [PhotoObject] {
        if let settings = UserDefaults.standard.value([PhotoObject].self, forKey: key) {
            return settings
        }
        return [PhotoObject]()
    }
    func setSettings(_ settings: [PhotoObject]) {
        UserDefaults.standard.set(encodable: settings, forKey: key)
    }
}
extension UserDefaults {

    func set<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }

    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
            let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
}
